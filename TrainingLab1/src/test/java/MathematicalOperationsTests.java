import com.progressoft.induction.mathmaticalOperations.MatrixMath;
import com.progressoft.induction.mathmaticalOperations.MatrixMathImpl;
import com.progressoft.induction.model.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class MathematicalOperationsTests {

    MatrixMath matrixMath;

    @BeforeEach
    void setUp() {
        matrixMath = new MatrixMathImpl();
    }

    //given_when_then

    @Test
    void givenEqualSizeOfMatrices_whenAdditionTwoMatrices_thenReturnNewMatrixOfSummation() {
        Matrix firstMatrix = new Matrix(new int[][]{{1, 3, 1}, {1, 0, 0}});
        Matrix secondMatrix = new Matrix(new int[][]{{0, 0, 5}, {7, 5, 0}});

        Matrix actualMatrix = matrixMath.sumOfTwoArrays(firstMatrix, secondMatrix);
        int[][] expectedMatrix = {{1, 3, 6}, {8, 5, 0}};

       assertArrayEquals(expectedMatrix, actualMatrix.getData());
    }

    @Test
    void givenNotEqualSizeOfMatrices_whenAdditionTwoMatrices_thenThrowIllegalArgumentException() {
        String expectedMessage = "dimensional arrays have not the same size";

        Matrix firstMatrix = new Matrix(new int[][]{{1, 3, 1}, {1, 0, 0}, {1, 0, 0}});
        Matrix secondMatrix = new Matrix(new int[][]{{0, 0, 5}, {7, 5, 0}});

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> matrixMath.sumOfTwoArrays(firstMatrix, secondMatrix));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void givenScaleNumberAndMatrix_whenMultiplyTheMatrixByScaleNumber_thenReturnNewScalableMatrix() {

        Matrix matrix = new Matrix(new int[][]{{1, 8, -3}, {4, -2, 5}});

        int[][] expectedMatrix = {{2, 16, -6}, {8, -4, 10}};
        Matrix actualMatrix = matrixMath.scalarMultiplication(matrix, 2) ;

        assertArrayEquals(expectedMatrix , actualMatrix.getData() );
    }

    @Test
    void givenMatrix_whenTransposeTheMatrix_thenReturnNewMatrixWithReversedRowAndColumn() {
        Matrix matrix = new Matrix(new int[][]{{1, 2, 3}, {0, -6, 7}});

        int[][] expectedMatrix = {{1, 0}, {2, -6}, {3, 7}};
        Matrix actualMatrix = matrixMath.transportation(matrix) ;

        //TODO: try assertArrayEqual
       assertArrayEquals(expectedMatrix , actualMatrix.getData() );
    }

    @Test
    void givenFirstMatrixWithRowSizeEqualToSecondMatrixWithColumnSize_whenMultiplyTheTwoMatrices_thenReturnNewMatrixOfMultiplication() {

        Matrix firstMatrix = new Matrix(new int[][]{{2, 1, 4}, {0, 1, 1}});
        Matrix secondMatrix = new Matrix(new int[][]{{6, 3, -1, 0}, {1, 1, 0, 4}, {-2, 5, 0, 2}});

        int[][] expectedMatrix = {{5, 27, -2, 12}, {-1, 6, 0, 6}};
        Matrix actualMatrix = matrixMath.matrixMultiplication(firstMatrix, secondMatrix) ;
        assertArrayEquals( expectedMatrix , actualMatrix.getData() );
    }

    @Test
    void givenFirstMatrixWithRowSizeNotEqualToSecondMatrixWithColumnSize_whenMultiplyTheTwoMatrices_thenReturnIllegalArgumentException() {
        String expectedMessage = "the number of columns of the first matrix is not the same as the number of rows of the second matrix.";

        Matrix firstMatrix = new Matrix(new int[][]{{2, 1, 4, 8}, {0, 1, 1, 4}});
        Matrix secondMatrix = new Matrix(new int[][]{{6, 3, -1, 0}, {1, 1, 0, 4}, {-2, 5, 0, 2}});

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> matrixMath.matrixMultiplication(firstMatrix, secondMatrix));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void givenMatrix_whenDoSubMatrix_thenReturnNewMatrix() {
        Matrix matrix = new Matrix(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}});

        int[][] expectedMatrix = {{1, 3, 4}, {5, 7, 8}};
        Matrix actualMatrix = matrixMath.subMatrix(matrix, 2, 1) ;

        assertArrayEquals(expectedMatrix, actualMatrix.getData());
    }

    @Test
    void givenDiagonalSquareTypeAndMatrix_whenImplementDiagonalTypeOnTheMatrix_thenReturnDiagonalMatrix() {
        Matrix matrix = new Matrix(new int[][]{{2, -3, 1}, {2, 1, -1}, {1, 4, 5}});

        int[][] expectedMatrix = {{2, 0, 0}, {0, 1, 0}, {0, 0, 5}};
        Matrix actualMatrix = matrixMath.squareMatrix(matrix, MatrixMathImpl.DIAGONAL);

        assertTrue(Arrays.deepEquals(expectedMatrix , actualMatrix.getData() ));
    }

    @Test
    void givenLowerTriangularSquareTypeAndMatrix_whenImplementLowerTriangularTypeOnOnTheMatrix_thenReturnLowerTriangularMatrix() {
        Matrix matrix = new Matrix(new int[][]{{2, -3, 1}, {2, 1, -1}, {1, 4, 5}});

        int[][] expectedMatrix = {{2, 0, 0}, {2, 1, 0}, {1, 4, 5}};
        Matrix actualMatrix = matrixMath.squareMatrix(matrix, MatrixMathImpl.LOWER_TRIANGULAR) ;

       assertArrayEquals(expectedMatrix , actualMatrix.getData());
    }

    @Test
    void givenUpperTriangularSquareTypeAndMatrix_whenImplementUpperTriangularTypeOnTheMatrix_thenReturnUpperTriangularMatrix() {
        Matrix matrix = new Matrix(new int[][]{{2, -3, 1}, {2, 1, -1}, {1, 4, 5}});

        int[][] expectedMatrix = {{2, -3, 1}, {0, 1, -1}, {0, 0, 5}};
        Matrix actualMatrix = matrixMath.squareMatrix(matrix, MatrixMathImpl.UPPER_TRIANGULAR) ;

       assertArrayEquals(expectedMatrix , actualMatrix.getData() );
    }

    @Test
    void givenUnknownSquareTypeAndMatrix_whenImplementUnknownTypeOnTheMatrix_thenThrowIllegalArgumentException() {
        String typeOfSource = "loop";
        String expected = typeOfSource + " is not supported";

        Matrix matrix = new Matrix(new int[][]{{1, 3, 1}, {3, 9, 5}, {0, 2, 1}, {0, 4, 2}});
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> matrixMath.squareMatrix(matrix, typeOfSource));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void given2x2Matrix_whenFindTheDeterminantOfTheMatrix_thenReturnTheValueOfDeterminant() {
        Matrix matrix = new Matrix(new int[][]{{1, 2}, {3, 4}});

        int expectedDeterminant = -2;
        int actualDeterminant = matrixMath.determinant(matrix) ;

        assertEquals(expectedDeterminant , actualDeterminant);

    }

    @Test
    void given3x3Matrix_whenFindTheDeterminantOfTheMatrix_thenReturnTheValueOfDeterminant() {
        Matrix matrix = new Matrix(new int[][]{{2, -3, 1}, {2, 0, -1}, {1, 4, 5}});

        int expectedDeterminant = 49;
        int actualDeterminant = matrixMath.determinant(matrix) ;

        assertEquals(expectedDeterminant , actualDeterminant );

    }

    @Test
    void given4x4Matrix_whenFindTheDeterminantOfTheMatrix_thenReturnTheValueOfDeterminant() {
        Matrix matrix = new Matrix(new int[][]{{1, 3, 1, 4}, {3, 9, 5, 15}, {0, 2, 1, 1}, {0, 4, 2, 3}});

        int expectedDeterminant = -4;
        int actualDeterminant = matrixMath.determinant(matrix) ;

        assertEquals(expectedDeterminant , actualDeterminant );

    }

    @Test
    void givenUnEqualTwoDimensionalMatrix_whenFindTheDeterminantOfTheMatrix_thenThrowIllegalArgumentException() {
        String expectedMsg = "columns and rows are not equals";

        Matrix matrix = new Matrix(new int[][]{{1, 3, 1}, {3, 9, 5}, {0, 2, 1}, {0, 4, 2}});
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> matrixMath.determinant(matrix));
        assertEquals(expectedMsg, exception.getMessage());

    }
}
