package com.progressoft.induction.model;

public class Matrix {

    private int [][] data ;
    private int columns ;
    private int rows ;

    public Matrix(int[][] data) {
        //TODO: set columns and rows based on data input
        this.data = data;
        this.columns = data[0].length;
        this.rows = data.length;
    }

    public int[][] getData() {
        return data;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
}
