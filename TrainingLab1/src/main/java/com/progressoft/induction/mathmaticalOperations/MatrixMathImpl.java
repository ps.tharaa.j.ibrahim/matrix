package com.progressoft.induction.mathmaticalOperations;
import com.progressoft.induction.model.Matrix;

public class MatrixMathImpl implements MatrixMath {

    public static final String DIAGONAL = "diagonal";
    public static final String LOWER_TRIANGULAR = "lower triangular";
    public static final String UPPER_TRIANGULAR = "upper triangular";

    @Override
    public Matrix sumOfTwoArrays(Matrix firstMatrix, Matrix secondMatrix) {
        //TODO: make the return type is Matrix
        if (firstMatrix.getColumns() != secondMatrix.getColumns() || firstMatrix.getRows() != secondMatrix.getRows()) {
            throw new IllegalArgumentException("dimensional arrays have not the same size");
        }
        Matrix sumOfRows = new Matrix(new int[firstMatrix.getRows()][firstMatrix.getColumns()]);

        for (int row = 0; row < firstMatrix.getRows(); row++) {
            for (int index = 0; index < firstMatrix.getColumns(); index++) {
                sumOfRows.getData()[row][index] = firstMatrix.getData()[row][index] + secondMatrix.getData()[row][index];
            }
        }
        return sumOfRows;
    }

    @Override
    public Matrix scalarMultiplication(Matrix matrix, int scaleNum) {

        Matrix scalableRows = new Matrix(new int[matrix.getRows()][matrix.getColumns()]);

        for (int row = 0; row < matrix.getRows(); row++) {
            for (int index = 0; index < matrix.getColumns(); index++) {
                scalableRows.getData()[row][index] = matrix.getData()[row][index] * scaleNum;
            }
        }
        return scalableRows;
    }

    @Override
    public Matrix transportation(Matrix matrix) {

        Matrix transMatrix = new Matrix(new int[matrix.getColumns()][matrix.getRows()]);

        for (int row = 0; row < matrix.getRows(); row++) {
            for (int index = 0; index < matrix.getColumns(); index++) {
                transMatrix.getData()[index][row] = matrix.getData()[row][index];
            }
        }
        return transMatrix;
    }

    @Override
    public Matrix matrixMultiplication(Matrix firstMatrix, Matrix secondMatrix) {

        if (firstMatrix.getColumns() != secondMatrix.getRows()) {
            throw new IllegalArgumentException("the number of columns of the first matrix is not the same as the number of rows of the second matrix.");
        }

        Matrix matrixMulti = new Matrix(new int[firstMatrix.getRows()][secondMatrix.getColumns()]);

        for (int row = 0; row < matrixMulti.getRows() ; row++) {
            for (int col = 0; col < matrixMulti.getColumns() ; col++) {
                matrixMulti.getData()[row][col] = multiplyCells(firstMatrix.getData(), secondMatrix.getData(), row, col);
            }
        }
        return matrixMulti;
    }

    @Override
    public Matrix subMatrix(Matrix matrix, int subRow, int subColumn) {

        Matrix subMatrix = new Matrix(new int[matrix.getRows() - 1][matrix.getColumns() - 1] );

        int subMatrixRow = 0;
        for (int row = 0; row < matrix.getRows(); row++) {

            if (row == subRow) continue;

            subMatrix = findSubColumns(matrix , subMatrix , subColumn , subMatrixRow , row );

            subMatrixRow++;
        }
        return subMatrix;
    }

    @Override
    public Matrix squareMatrix(Matrix matrix, String typeOfSquare) {

        Matrix squareMatrix = new Matrix(matrix.getData());

        for (int row = 0; row < squareMatrix.getRows(); row++) {
            for (int index = 0; index < squareMatrix.getColumns() ; index++) {

               detectSquareType(typeOfSquare , squareMatrix , row , index) ;

            }
        }
        return squareMatrix;
    }

    @Override
    public int determinant(Matrix matrix) {
        isSquareMatrix(matrix);

        int determinant = 0;

        //Base condition
        if (matrix.getColumns() == 2 && matrix.getRows() == 2) {
            return squareDeterminant(matrix.getData());
        }

        for (int col = 0; col < matrix.getColumns(); col++) {
            Matrix subMatrix = new Matrix(subMatrix(matrix, 0, col).getData());
            if (col % 2 == 0) determinant = determinant + determinant(subMatrix) * matrix.getData()[0][col];
            else determinant = determinant - determinant(subMatrix) * matrix.getData()[0][col];
        }

        return determinant;
    }

    private int multiplyCells(int[][] firstMatrix, int[][] secondMatrix, int row, int col) {
        int cell = 0;
        for (int index = 0; index < secondMatrix.length; index++) {
            cell += firstMatrix[row][index] * secondMatrix[index][col];
        }
        return cell;
    }

    private int squareDeterminant(int[][] matrix) {
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }

   private void detectSquareType(String typeOfSquare , Matrix squareMatrix , int row , int index){

       switch (typeOfSquare) {
           case DIAGONAL:
               if (row != index) squareMatrix.getData()[row][index] = 0;
               break;
           case LOWER_TRIANGULAR:
               if (row < index) squareMatrix.getData()[row][index] = 0;
               break;
           case UPPER_TRIANGULAR:
               if (row > index) squareMatrix.getData()[row][index] = 0;
               break;
           default:
               throw new IllegalArgumentException(typeOfSquare + " is not supported");
       }
   }

   private void isSquareMatrix(Matrix matrix){
       if (matrix.getRows() != matrix.getColumns()) {
           throw new IllegalArgumentException("columns and rows are not equals");
       }

   }

   private Matrix findSubColumns(Matrix matrix , Matrix subMatrix  , int subColumn ,  int subMatrixRow , int row){
        int subMatrixCol = 0 ;
        Matrix subMatrix1 = new Matrix(subMatrix.getData()) ;

       for (int col = 0; col < matrix.getColumns(); col++) {
           if (col == subColumn) continue;
           subMatrix1.getData()[subMatrixRow][subMatrixCol] = matrix.getData()[row][col];
           subMatrixCol++;
       }
       return subMatrix1 ;
   }
}
