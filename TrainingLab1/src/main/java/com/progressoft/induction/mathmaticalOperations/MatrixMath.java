package com.progressoft.induction.mathmaticalOperations;

import com.progressoft.induction.model.Matrix;

public interface MatrixMath {

    int determinant(Matrix matrix);

    //TODO: rename method name
    Matrix sumOfTwoArrays(Matrix firstMatrix, Matrix secondMatrix);

    Matrix scalarMultiplication(Matrix matrix, int scaleNum);

    Matrix transportation(Matrix matrix);

    Matrix matrixMultiplication(Matrix firstMatrix, Matrix secondMatrix);

    Matrix subMatrix(Matrix matrix, int subRow, int subColumn);

    Matrix squareMatrix(Matrix matrix, String typeOfSquare);
}
